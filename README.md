### How do I get set up? ###

* Build Circuit 
* Compile and deploy code to Arduino Uno
* Push button to switch between C and F

[Fritzing Project Page](http://fritzing.org/projects/temperature-display)

### Diagram ###

![Wiring Diagram](http://fritzing.org/media/fritzing-repo/projects/t/temperature-display/images/TempDisplay_bb.png "Wiring Diagram")