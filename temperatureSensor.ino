const int digits [4] = {0,1,2,3};
const int segments [7]= {4,5,6,7,8,9,10};
const int dPoint = 12;
const int buttonPin = A0;
const int tempSensorPin = A5;

const int digitEnable = LOW;
const int digitDisable = HIGH;
const int segmentEnable = digitDisable;
const int segmentDisable = digitEnable;

int ONE [] = {2,6,16};
int TWO [] = {1,2,3,4,5,16};
int THREE [] = {1,2,3,5,6,16};
int FOUR [] = {0,2,3,6,16};
int FIVE [] = {0,1,3,5,6,16};
int SIX [] = {0,1,3,4,5,6,16};
int SEVEN [] = {0,1,2,6,16};
int EIGHT [] = {0,1,2,3,4,5,6,16};
int NINE [] = {0,1,2,3,5,6,16};
int ZERO [] = {0,1,2,4,5,6,16};

bool showF = true;
bool buttonPressed = false;

void setup() {
    //initialize pins

    pinMode(buttonPin, INPUT);
    pinMode(tempSensorPin, INPUT);

    for (int digitIndex = 0; digitIndex < 4; ++digitIndex) {
        pinMode(digits[digitIndex], OUTPUT);
        digitalWrite(digits[digitIndex], digitEnable);
    }

    for (int segmentIndex = 0; segmentIndex < 7; ++segmentIndex) {
        pinMode(segments[segmentIndex], OUTPUT);
        digitalWrite(segments[segmentIndex], segmentEnable);
    }

    pinMode(dPoint, OUTPUT);
    digitalWrite(dPoint, segmentEnable);
    //delay(2000);
    digitalWrite(dPoint, segmentDisable);

    for (int digitIndex = 0; digitIndex < 4; ++digitIndex) {
        digitalWrite(digits[digitIndex], digitDisable);
    }

}

void loop() {
    int tempSensorValue = analogRead(tempSensorPin);
    float millivolts = (tempSensorValue / 1024.0) * 5000;
    float cel = millivolts / 10;
    float f = (cel * 9 / 5) + 32;

    checkForButtonPress();
    if (showF) {
        writeNumber(f);
    }
    else {
        writeNumber(cel);
    }
}

void writeNumber(float num) {
    writeNumber(num, 5);
}

void checkForButtonPress() {
    int buttonState = analogRead(buttonPin);
    if (buttonState > 512) {
        if (!buttonPressed) {
            showF = !showF;
        }
        buttonPressed = true;
    }
    else {
        buttonPressed = false;
    }
}

void blinkOutput() {
    pinMode(13, OUTPUT);
    digitalWrite(13, HIGH);
    delay(250);
    digitalWrite(13, LOW);
    delay(250);
}

void test() {
    for (int digitIndex = 0; digitIndex < 4; ++digitIndex) {
        digitalWrite(digits[digitIndex], digitEnable);

        for (int segmentIndex = 0; segmentIndex < 7; ++segmentIndex) {
            digitalWrite(segments[segmentIndex], segmentEnable);
            delay(250);
            digitalWrite(segments[segmentIndex], segmentDisable);
        }
        digitalWrite(dPoint, segmentEnable);
        delay(250);
        digitalWrite(dPoint, segmentDisable);
        digitalWrite(digits[digitIndex], digitDisable);
    }

}

void writeNumber(float number, int times) {
    int shifts = 1;
    while (number < 1000) {
        number = number * 10;
        ++shifts;
        if (shifts > 4) {
            break;
        }
    }
    shifts = 4 - shifts;

    int intNum = number;
    int ones = intNum % 10;
    int tens = (intNum / 10) % 10;
    int hundreds = (intNum / 100) % 10;
    int thousands = (intNum / 1000) % 10;

    for (int delayCounter = 0; delayCounter < times; ++delayCounter) {
        writeDigit(ones, 3, shifts);
        if (intNum > 9) {
            writeDigit(tens, 2, shifts);
        }
        if (intNum > 99) {
            writeDigit(hundreds, 1, shifts);
        }
        if (intNum > 999) {
            writeDigit(thousands, 0, shifts);
        }
    }
}

void writeDigit(int number, int digit, int shifts) {
    //get mapping for number
    int* mapping = getNumberMapping(number);

    //enable new digit
    digitalWrite(digits[digit], digitEnable);

    //check decimal
    if (digit == shifts) {
        digitalWrite(dPoint, segmentEnable);
    }

    //turn on segments
    for (int mappingIndex = 0; mappingIndex < 7; ++mappingIndex) {
        if (mapping[mappingIndex] > 6) {
            break;
        }
        digitalWrite(segments[mapping[mappingIndex]], segmentEnable);
    }

    delay(5);
    //turn off segments
    for (int s = 0; s < 7; ++s) {
        digitalWrite(segments[s], segmentDisable);
    }

    //turn off dpoint
    digitalWrite(dPoint, segmentDisable);

    //turn off digit
    digitalWrite(digits[digit], digitDisable);
}

int* getNumberMapping(int number) {
    int* returnVal;
    switch(number) {
        case 1:
            return ONE;
        case 2:
            return TWO;
        case 3:
            return THREE;
        case 4:
            return FOUR;
        case 5:
            return FIVE;
        case 6:
            return SIX;
        case 7:
            return SEVEN;
        case 8:
            return EIGHT;
        case 9:
            return NINE;
        case 0:
            return ZERO;
    }
}


